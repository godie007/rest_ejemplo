'use strict'

exports = module.exports = function (app, mongoose) {
  var appointmentTypeSchema = new mongoose.Schema({
    type: {type: String, enum: ['prioritaria', 'general']},
    createdAt: {type: Date, default: Date.now},
    updateAt: String
  })
  appointmentTypeSchema.set('autoIndex', (app.get('env') === 'development'))
  app.db.model('appointmentType', appointmentTypeSchema)
}
