'use strict'
/**
* @autor:godie007
* @date: 2017/07/23 19:49
* Implementation of the POST method for the appointmentType collection
**/

exports.ingresar = (req, res) => {
  const item = req.body
  console.log(item)
  req.app.db.models.appointmentType.create(item, (err, data) => {
    if (err) {
      console.log('fallo al ingresar! ' + err)
      res.send(['ERROR'])
    } else {
      console.log(data)
      res.send(['OK'])
    }
  })
}
/**
* @autor:godie007
* @date: 2017/07/23 19:55
* Implementation of the GET method for the appointmentType collection
**/
exports.mostrar = (req, res) => {
  res.render('AppointmentTypes/vista', {'params': {'title': 'Ejemplo', 'head': 'Tipo de Cita NodeJS'}})
}

exports.listar = (req, res) => {
  req.app.db.models.appointmentType.find({}, (err, data) => {
    console.log(data)
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
  })
}
exports.buscar = (req, res) => {
  const id = req.params.id
  req.app.db.models.appointmentType.findOne({'_id': id}, (err, data) => {
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
  })
}

/**
* @autor: Group 1
* @date: 2017/08/08 15:02
* Implementation of the delete method for the appointmentType collection
**/
exports.borrar = (req, res) => {
  const id = req.body.id
  console.log('id', id)
  req.app.db.models.appointmentType.remove({'_id': id}, (err, resp) => {
    if (err) {
      res.send(['ERROR'])
    } else {
      res.send(['OK'])
    }
  })
}

/**
* @autor: Jhon
* @date: 2017/08/08 15:05
* Implementation of the PUT method for the appointmentType collection
**/
exports.update = (req, res) => {
  const item = req.body
  console.log(item)
  req.app.db.models.appointmentType.update(item, (err, data) => {
    if (err) {
      console.log('fallo al actualizar! ' + err)
      res.send(['ERROR'])
    } else {
      res.send(['OK'])
    }
  })
}
