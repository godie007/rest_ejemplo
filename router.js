exports = module.exports = function (app) {
  app.get('/users/', require('./views/Users/index').mostrar)
  app.post('/users/', require('./views/Users/index').ingresar)
  app.post('/users/listar/', require('./views/Users/index').listar)
  app.put('/users/', require('./views/Users/index').update)
  app.delete('/users/', require('./views/Users/index').borrar)
  app.post('/users/:id', require('./views/Users/index').buscar)

  app.get('/appoinments/', require('./views/Appoinments/index').mostrar)
  /* app.post('/appoinments/', require('./views/Appoinments/index').ingresar)
  app.put('/appoinments/', require('./views/Appoinments/index').actualizar)
  app.delete('/appoinments/', require('./views/Appoinments/index').borrar)
  */

  app.get('/appoTypes/', require('./views/AppointmentTypes/index').mostrar)
  app.post('/appoTypes/', require('./views/AppointmentTypes/index').ingresar)
  app.post('/appoTypes/listar/', require('./views/AppointmentTypes/index').listar)
  app.put('/appoTypes/', require('./views/AppointmentTypes/index').update)
  app.delete('/appoTypes/', require('./views/AppointmentTypes/index').borrar)
  app.post('/appoTypes/:id', require('./views/AppointmentTypes/index').buscar)

}
